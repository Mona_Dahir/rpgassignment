﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstAssignment.Characters
{
   public class Warrior: Heroes
    {

        public Warrior()
        {
            BasePrimaryAttributes = new PrimaryAttribute() { Strength = 5, Dexterity = 2, Intelligence = 1, Vitality = 10 };
            LevelUp = new PrimaryAttribute() { Strength = 3, Dexterity = 2, Intelligence = 1, Vitality = 5 };
            ValidWeopon = new List<Weapon.WeaponTypes> { Weapon.WeaponTypes.Axes, Weapon.WeaponTypes.Hammers, Weapon.WeaponTypes.Swords };
            ValidArmor = new List<Armor.ArmorTypes> { Armor.ArmorTypes.Plate, Armor.ArmorTypes.Mail };
            TotalAttributes();
            AllSecondareAttributes(TotalPrimaryAttributes);
        }
    
        public override double CharacterDamage()
        {
            for (int i = 1; i < TotalPrimaryAttributes.Strength; i++)
            {

                TotalPrimaryAttributes.Strength *= Convert.ToInt32(1.01);

            }
            return TotalPrimaryAttributes.Strength;
        }
        public override string ToString()
        {
            return base.ToString();
        }
    }
}
