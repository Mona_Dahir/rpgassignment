﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstAssignment.Characters
{
    public class Ranger : Heroes
    {

        public Ranger ()
        {
            BasePrimaryAttributes = new PrimaryAttribute() { Strength = 1, Dexterity = 7, Intelligence = 1, Vitality = 8 };
            LevelUp = new PrimaryAttribute() { Strength = 1, Dexterity = 5, Intelligence = 1, Vitality = 2 };
            ValidArmor = new List<Armor.ArmorTypes> { Armor.ArmorTypes.Leather, Armor.ArmorTypes.Mail };
            ValidWeopon = new List<Weapon.WeaponTypes> { Weapon.WeaponTypes.Bows };
            TotalAttributes();
            AllSecondareAttributes(TotalPrimaryAttributes);
        }
      
        public override double CharacterDamage()
        {
            for (int i = 0; i < TotalPrimaryAttributes.Dexterity; i++)
            {
                TotalPrimaryAttributes.Dexterity *= Convert.ToInt32(1.01);

            }
            return TotalPrimaryAttributes.Dexterity;
        }
        public override string ToString()
        {
            return base.ToString();
        }
    }
}
