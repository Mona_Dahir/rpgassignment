﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstAssignment.Characters
{
    public abstract class Heroes
    {
        public string Name { get; set; }
        public int Level { get; set; }
        public PrimaryAttribute LevelUp  { get; set; } = new PrimaryAttribute();
        public PrimaryAttribute BasePrimaryAttributes { get; set; }
        public PrimaryAttribute TotalPrimaryAttributes { get; set; } = new PrimaryAttribute();
        public SecondaryAttribute SecondaryAttribute { get; set; } = new SecondaryAttribute();
        public List<Weapon.WeaponTypes> ValidWeopon{ get; set; }
        public List<Armor.ArmorTypes> ValidArmor { get; set; }
        private Dictionary<Slot, Item> SlotsItems { get; set; } = new Dictionary<Slot, Item>();

        public Heroes()

        {
            Level = 1;
            SlotsItems.Add(Slot.Weapon, null);
            SlotsItems.Add(Slot.Body, null);
            SlotsItems.Add(Slot.Head, null);
            SlotsItems.Add(Slot.Legs, null);

        }

        //<summary>
        /// Calculates the individual character damage by taking the toatal primary attribute and the increase the individual damage, it then returns the new total primaryattribute 
        //<summary
        public abstract double CharacterDamage();

        //<summary>
        /// Returns a string of the indivial chracters primaryattributes and the calculated secondaryattributes
        //<summary
        public override string ToString()
        {
           return $"Name:{Name} \tLevel:{Level} \tStrenght:{TotalPrimaryAttributes.Strength}  \tDexerity:{TotalPrimaryAttributes.Dexterity} \tIntelligence:{TotalPrimaryAttributes.Intelligence} \tHealth:{SecondaryAttribute.Health} \tArmor Rating:{SecondaryAttribute.ArmorRating} \tElement resistance:{SecondaryAttribute.ElementalResistance} \tDPS:{ CharacterDPS()}";  

        }

        //<summary>
        /// Takes in an int for level to calculate and add the primary attributes the individual character gains, the gains are then added to total primaattributes
        /// calls the AllSeondaryAttributes method to update the secondary attributes 
        /// ads and returns the new level
        //<summary
        public int LevelUP (int Level)
        {
            if(Level < 1)
            {
                throw new ArgumentException("Too low level");
            }
            else
            {

                for (int i = 1; i < Level +1; i++)
                {
                    TotalPrimaryAttributes.Intelligence += LevelUp.Intelligence;
                    TotalPrimaryAttributes.Dexterity += LevelUp.Dexterity;
                    TotalPrimaryAttributes.Strength += LevelUp.Strength;
                    TotalPrimaryAttributes.Vitality +=  LevelUp.Vitality;

                }

                AllSecondareAttributes(TotalPrimaryAttributes);
                return this.Level += Level;

            }

        }

        //<Summary>
        //Takes in the primary attributes and then calculates the secondary attribute
        //<Summary>
        public void AllSecondareAttributes(PrimaryAttribute primaryAttribute)
        {
            SecondaryAttribute.Health = TotalPrimaryAttributes.Vitality * 10;
            SecondaryAttribute.ArmorRating = TotalPrimaryAttributes.Strength + TotalPrimaryAttributes.Dexterity;
            SecondaryAttribute.ElementalResistance = TotalPrimaryAttributes.Intelligence;

        }

        //<Summary>
        // Adds the characters base Primary attributes to total attributes 
        //<Summary>
        public void TotalAttributes()
        {
            TotalPrimaryAttributes.Dexterity += BasePrimaryAttributes.Dexterity;
            TotalPrimaryAttributes.Strength += BasePrimaryAttributes.Strength;
            TotalPrimaryAttributes.Vitality += BasePrimaryAttributes.Vitality;
            TotalPrimaryAttributes.Intelligence += BasePrimaryAttributes.Intelligence;

        }
        //<Summary>
        //Check if a weapon is equiped, if there is no weapon return defual value 1,
        //if weapon is equiped call method to calculate weapon damage and method to calculate the individual character damage to calculate the chracter damage
        //<Summary>
        public double CharacterDPS()
        {          
            Weapon weapon = new Weapon();
            weapon = (Weapon)SlotsItems[Slot.Weapon];

            if (SlotsItems[Slot.Weapon] is not null)
            {

                double CharacterD = Convert.ToDouble(CharacterDamage());

                return weapon.DamagePerSecond() * (1 + CharacterD / 100);

            }
            else
            {
                return 1;
            }
        }

        #region Check if Armor is equipble, if true equip armor and add to slot 

        //<Summary>
        //Equips armor 
        //Checks first if armor slot is empty if, if not empty throw excepton that chrachter already has armor equipte
        //if is empty then call method to check if it is valid armor for the specific character, if its not a valid armor and wrong armor for chracter then an massage is throws
        //if its valid armor then check so slot is not weapon slot, if its weapon slot then an exception that is wrong slottyp is thrown
        //if its not weapon slot then armor is equiped and its added to the armor slot and method that adds the armor to the total primary attributes is called and ads the armor
        //<Summary>
        public string EquipArmor(Armor armor)
        {
            if (SlotsItems[armor.ItemSlot] is null)
            {
                if (CheckValidArmor(armor))
                {
                    if(armor.ItemSlot == Slot.Weapon)
                    {
                        throw new Exception("Invalid slottype");
                    }
                    else
                    {
                        SlotsItems[armor.ItemSlot] = armor;
                        TotalAttributePlusArmor(armor);
                        return "New armor equiped";
                    }
                }
                else
                {
                    return "cant equip " + armor.ItemName + "you're to low level";
                }
            }
            else
            {
                return "you already have an armor equipt";
            }
           
        }

        //<Summary>
        // Takes in armor and adds the armor to the total primary attributes
        //<Summary>
        public void TotalAttributePlusArmor(Armor ArmorAttributes)
        {
            TotalPrimaryAttributes.Dexterity += ArmorAttributes.attributes.Dexterity;
            TotalPrimaryAttributes.Strength += ArmorAttributes.attributes.Strength;
            TotalPrimaryAttributes.Vitality += ArmorAttributes.attributes.Vitality;
            TotalPrimaryAttributes.Intelligence += ArmorAttributes.attributes.Intelligence;
        }

        //<Summary>
        // Checks if the armor the character wants to equip is valid for the specefic chracter and if its the righ level
        // if its not a valid armor an InvalidArmorException is thrown 
        // if its valid returns true and armor is equiped in EquipArmor method
        //<Summary>
        public bool CheckValidArmor (Armor armor)
        {
            try
            {
                if ((ValidArmor.Where(ArmorType => armor.armorType == ArmorType).ToList().Count == 1)  && (armor.ItemLevel <= Level)) return true;
                else
                {
                    throw new InvalidArmorType();
                }
            }
            catch (InvalidArmorType)
            {
                
                throw new InvalidArmorType("Invalid armor, You cant equip this armor, too low level or wrong level armortype");             
            }
        }

        #endregion

        #region Check if weapon is equipble, if true equip weapon and add to slot weapon

        //<Summary>
        //Equips weapon 
        //Checks first if weapon slot is empty if, if not empty throw excepton that character already has a weapon equipte
        //if it is empty then call method to check if it is valid weapon for the specific character with input weapon as a parameter, if its not a valid weapon for the character then an massage is printed
        //if its valid weapon then check if slot is weapon, if wrong slot exception is thrown
        //if its the right slot then weapon is equiped and its added to the weapon slot
        //<Summary>
        public string EquipWeapon(Weapon weapon )
        {
            if (SlotsItems[weapon.ItemSlot] is null) 
            {
                if (CheckValidWeapon(weapon))
                {
                    if (weapon.ItemSlot == Slot.Weapon)
                    {
                        SlotsItems[weapon.ItemSlot] = weapon;
                        return "New weapon equiped";
                    }
                    else
                    {
                        throw new Exception("Invalid slottype");
                    }
                }
                else
                {
                    return "cant equip " + weapon.ItemName;
                }
            }
            else
            {
                return "You already have a weapon equipment";
            }
        }

        //<Summary>
        // Checks if the weapon the character wants to equip is valid for the specefic chracter and if its the righ level
        // if its not a valid weapon an InvalidWeaponException is thrown 
        // if its valid returns true and wepon is equiped in EquipWeapon method
        //<Summary>
        public bool CheckValidWeapon(Weapon weapon)
        {
            try
            {

                if ((ValidWeopon.Where(WeaponType => weapon.WeaponType == WeaponType).ToList().Count == 1) && (weapon.ItemLevel <= this.Level)) return true;
                else
                {
                    throw new InvalidWeaponType();
                }
            }
            catch (InvalidArmorType)
            {
                throw new InvalidArmorType("Invalid weapon, You cant equip this weapon, too low level or wrong level weapontype");
            }
        }
        #endregion 
    }
}
