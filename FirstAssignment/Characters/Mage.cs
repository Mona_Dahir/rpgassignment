﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstAssignment
{
    public class Mage : Characters.Heroes
    {

        public Mage()
        {
            ValidWeopon = new List<Weapon.WeaponTypes> { Weapon.WeaponTypes.Staffs, Weapon.WeaponTypes.Wands };
            ValidArmor  = new List<Armor.ArmorTypes> { Armor.ArmorTypes.Cloth };
            BasePrimaryAttributes = new PrimaryAttribute() { Strength = 1, Dexterity = 1, Intelligence = 8, Vitality = 5 };
            LevelUp = new PrimaryAttribute() { Strength = 1, Dexterity = 1, Intelligence = 5, Vitality = 3 };
            TotalAttributes();
            AllSecondareAttributes(TotalPrimaryAttributes);

        }

        public override double CharacterDamage()
        {
            for (int i = 0; i < TotalPrimaryAttributes.Intelligence; i++)
            {
                TotalPrimaryAttributes.Intelligence *= Convert.ToInt32(1.01);

            }
            return TotalPrimaryAttributes.Intelligence;
        }
        public override string ToString()
        {
            return base.ToString();
        }
    }

}
