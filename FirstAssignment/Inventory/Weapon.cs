﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstAssignment
{
    public class Weapon : Item
      
    {
        public WeaponAttributes WeaponAttributes { get; set; }
        public WeaponTypes WeaponType { get; set; }

        public enum WeaponTypes
        {
            Axes,
            Bows,
            Dagers,
            Hammers,
            Staffs,
            Swords,
            Wands
        }

        //<Summary>
        // calculate the weapon damage per second usig obj from weaponattribute class
        //<Summary>
        public double DamagePerSecond()
        {
            return WeaponAttributes.Damage * WeaponAttributes.Attackspeed;
        }
       
    }
}
