﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstAssignment
{
    public class WeaponAttributes
    {
        public Double Damage { get; set; }
        public double Attackspeed { get; set; }
    }
}
