﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstAssignment
{
    public class Armor: Item
    {
        public PrimaryAttribute attributes { get; set; }
        public ArmorTypes armorType { get; set; }

        public enum ArmorTypes
        {
           Cloth,
           Leather,
           Mail,
           Plate
        }

    }
}
