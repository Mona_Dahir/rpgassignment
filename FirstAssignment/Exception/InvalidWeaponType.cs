﻿using System;
using System.Runtime.Serialization;

namespace FirstAssignment.Characters
{
    [Serializable]
    public class InvalidWeaponType : Exception
    {
        public InvalidWeaponType()
        {
        }

        public InvalidWeaponType(string message) : base(message)
        {
        }

        public InvalidWeaponType(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidWeaponType(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}