﻿using System;
using System.Runtime.Serialization;

namespace FirstAssignment.Characters
{
    [Serializable]
    public class InvalidArmorType : Exception
    {
        public InvalidArmorType()
        {
        }

        public InvalidArmorType(string message) : base(message)
        {
        }

        public InvalidArmorType(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidArmorType(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}