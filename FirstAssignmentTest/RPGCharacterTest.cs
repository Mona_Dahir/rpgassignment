using System;
using Xunit;
using FirstAssignment;
using FirstAssignment.Characters;

namespace FirstAssignmentTest
{
    public class RPGCharacterTest
    {
        Mage mage = new Mage();
        Warrior warrior = new Warrior();
        Rogue rogue = new Rogue();
        Ranger ranger = new Ranger();

        #region Check base level, level upp status and exception for invalid level
        [Fact]
        public void Character_Level_ReturnBaseLevel()
        {
            //Arrange
            int lhs = 1;
            int expected = lhs;

            //Act
            int actualMage = mage.Level;
            int actualWarrior = warrior.Level;
            int actualRogue = rogue.Level;
            int actualRanger = ranger.Level;

            //Assert
            Assert.Equal(expected, actualMage);
            Assert.Equal(expected, actualRogue);
            Assert.Equal(expected, actualWarrior);
            Assert.Equal(expected, actualRanger);

        }

        [Fact]
        public void LevelUp_Level_ReturnNewLevel()
        {
            //Arrange
            int lhs = 1;
            int rhs = 2;
            int expected = rhs;

            //Act
            int actualMage = mage.LevelUP(lhs);
            int actualWarrior = warrior.LevelUP(lhs);
            int actualRogue = rogue.LevelUP(lhs);
            int actualRanger = ranger.LevelUP(lhs);

            //Assert
            Assert.Equal(expected, actualMage);
            Assert.Equal(expected, actualRogue);
            Assert.Equal(expected, actualWarrior);
            Assert.Equal(expected, actualRanger);
        }

        [Theory]
        [InlineData(0, -1)]
        public void LevelUP_NonValidLevel_ReturnArgumentExeception(int lhs, int rhs)
        {
            //Act & Assert
            Assert.Throws<ArgumentException>(() => mage.LevelUP(lhs));
            Assert.Throws<ArgumentException>(() => mage.LevelUP(rhs));

            Assert.Throws<ArgumentException>(() => rogue.LevelUP(lhs));
            Assert.Throws<ArgumentException>(() => rogue.LevelUP(rhs));

            Assert.Throws<ArgumentException>(() => ranger.LevelUP(lhs));
            Assert.Throws<ArgumentException>(() => ranger.LevelUP(rhs));

            Assert.Throws<ArgumentException>(() => warrior.LevelUP(lhs));
            Assert.Throws<ArgumentException>(() => warrior.LevelUP(rhs));
        }

        #endregion

        #region Create all character with primare attributes and secondare attributes
        [Fact]
        public void CreateMage_Primaryattributes_ReturnBaseAttributes()
        {
            int expectedLevel = 1;
            PrimaryAttribute expectedPrimAttributes = new PrimaryAttribute();
            expectedPrimAttributes.Strength = 1;
            expectedPrimAttributes.Dexterity = 1;
            expectedPrimAttributes.Intelligence= 8;
            expectedPrimAttributes.Vitality = 5;

            SecondaryAttribute expectedSecondaryAttributes = new SecondaryAttribute();
            expectedSecondaryAttributes.Health = 50;
            expectedSecondaryAttributes.ArmorRating = 2;
            expectedSecondaryAttributes.ElementalResistance = 8;
            
            //Act
            int actualMageLevel = mage.Level;
            int actualMagePrimAtribuesVitality = mage.BasePrimaryAttributes.Vitality;
            int actualMagePrimAtribuesIntelligence = mage.BasePrimaryAttributes.Intelligence;
            int actualMagePrimAtribuesStrenght = mage.BasePrimaryAttributes.Strength;
            int actualMagePrimAtribuesDexterity = mage.BasePrimaryAttributes.Dexterity;

            int actualMageSecondaryAttributesHealth = mage.SecondaryAttribute.Health;
            int actualMageSecondaryAttributesArmorRating = mage.SecondaryAttribute.ArmorRating;
            int actualMageSecondaryAttributesElementResistance = mage.SecondaryAttribute.ElementalResistance;


            //Assert
            Assert.Equal(expectedLevel, actualMageLevel);

            Assert.Equal(expectedPrimAttributes.Vitality, actualMagePrimAtribuesVitality);
            Assert.Equal(expectedPrimAttributes.Intelligence, actualMagePrimAtribuesIntelligence);
            Assert.Equal(expectedPrimAttributes.Strength, actualMagePrimAtribuesStrenght);
            Assert.Equal(expectedPrimAttributes.Dexterity, actualMagePrimAtribuesDexterity);

            Assert.Equal(expectedSecondaryAttributes.Health, actualMageSecondaryAttributesHealth);
            Assert.Equal(expectedSecondaryAttributes.ArmorRating, actualMageSecondaryAttributesArmorRating);
            Assert.Equal(expectedSecondaryAttributes.ElementalResistance, actualMageSecondaryAttributesElementResistance);

        }

        [Fact]
        public void CreateRanger_Primaryattributes_ReturnBaseAttributes()
        {
            int expectedLevel = 1;
            PrimaryAttribute expectedPrimAttributes = new PrimaryAttribute();
            expectedPrimAttributes.Strength = 1;
            expectedPrimAttributes.Dexterity = 7;
            expectedPrimAttributes.Intelligence = 1;
            expectedPrimAttributes.Vitality = 8;

            SecondaryAttribute expectedSecondaryAttributes = new SecondaryAttribute();
            expectedSecondaryAttributes.Health = 80;
            expectedSecondaryAttributes.ArmorRating = 8;
            expectedSecondaryAttributes.ElementalResistance = 1;

            //Act
            int actualLevel = ranger.Level;
            int actualPrimAtribuesVitality = ranger.BasePrimaryAttributes.Vitality;
            int actualPrimAtribuesIntelligence = ranger.BasePrimaryAttributes.Intelligence;
            int actualPrimAtribuesStrenght = ranger.BasePrimaryAttributes.Strength;
            int actualPrimAtribuesDexterity = ranger.BasePrimaryAttributes.Dexterity;

            int actualSecondaryAttributesHealth = ranger.SecondaryAttribute.Health;
            int actualSecondaryAttributesArmorRating = ranger.SecondaryAttribute.ArmorRating;
            int actualSecondaryAttributesElementResistance = ranger.SecondaryAttribute.ElementalResistance;


            //Assert
            Assert.Equal(expectedLevel, actualLevel);

            Assert.Equal(expectedPrimAttributes.Vitality, actualPrimAtribuesVitality);
            Assert.Equal(expectedPrimAttributes.Intelligence, actualPrimAtribuesIntelligence);
            Assert.Equal(expectedPrimAttributes.Strength, actualPrimAtribuesStrenght);
            Assert.Equal(expectedPrimAttributes.Dexterity, actualPrimAtribuesDexterity);

            Assert.Equal(expectedSecondaryAttributes.Health, actualSecondaryAttributesHealth);
            Assert.Equal(expectedSecondaryAttributes.ArmorRating, actualSecondaryAttributesArmorRating);
            Assert.Equal(expectedSecondaryAttributes.ElementalResistance, actualSecondaryAttributesElementResistance);

        }

        [Fact]
        public void CreateRouge_Primaryattributes_ReturnBaseAttributes()
        {
            int expectedLevel = 1;
            PrimaryAttribute expectedPrimAttributes = new PrimaryAttribute();
            expectedPrimAttributes.Strength = 2;
            expectedPrimAttributes.Dexterity = 6;
            expectedPrimAttributes.Intelligence = 1;
            expectedPrimAttributes.Vitality = 8;

            SecondaryAttribute expectedSecondaryAttributes = new SecondaryAttribute();
            expectedSecondaryAttributes.Health = 80;
            expectedSecondaryAttributes.ArmorRating = 8;
            expectedSecondaryAttributes.ElementalResistance = 1;

            //Act
            int actualLevel = rogue.Level;
            int actualPrimAtribuesVitality = rogue.BasePrimaryAttributes.Vitality;
            int actualPrimAtribuesIntelligence = rogue.BasePrimaryAttributes.Intelligence;
            int actualPrimAtribuesStrenght = rogue.BasePrimaryAttributes.Strength;
            int actualPrimAtribuesDexterity = rogue.BasePrimaryAttributes.Dexterity;

            int actualSecondaryAttributesHealth = rogue.SecondaryAttribute.Health;
            int actualSecondaryAttributesArmorRating = rogue.SecondaryAttribute.ArmorRating;
            int actualSecondaryAttributesElementResistance = rogue.SecondaryAttribute.ElementalResistance;


            //Assert
            Assert.Equal(expectedLevel, actualLevel);

            Assert.Equal(expectedPrimAttributes.Vitality, actualPrimAtribuesVitality);
            Assert.Equal(expectedPrimAttributes.Intelligence, actualPrimAtribuesIntelligence);
            Assert.Equal(expectedPrimAttributes.Strength, actualPrimAtribuesStrenght);
            Assert.Equal(expectedPrimAttributes.Dexterity, actualPrimAtribuesDexterity);

            Assert.Equal(expectedSecondaryAttributes.Health, actualSecondaryAttributesHealth);
            Assert.Equal(expectedSecondaryAttributes.ArmorRating, actualSecondaryAttributesArmorRating);
            Assert.Equal(expectedSecondaryAttributes.ElementalResistance, actualSecondaryAttributesElementResistance);

        }

        [Fact]
        public void CreateWarrior_Primaryattributes_ReturnBaseAttributes()
        {
            //Arrge
            int expectedLevel = 1;
            PrimaryAttribute expectedPrimAttributes = new PrimaryAttribute();
            expectedPrimAttributes.Strength = 5;
            expectedPrimAttributes.Dexterity = 2;
            expectedPrimAttributes.Intelligence = 1;
            expectedPrimAttributes.Vitality = 10;

            SecondaryAttribute expectedSecondaryAttributes = new SecondaryAttribute();
            expectedSecondaryAttributes.Health = 100;
            expectedSecondaryAttributes.ArmorRating = 7;
            expectedSecondaryAttributes.ElementalResistance = 1;

            //Act
            int actualLevel = warrior.Level;
            int actualPrimAtribuesVitality = warrior.BasePrimaryAttributes.Vitality;
            int actualPrimAtribuesIntelligence = warrior.BasePrimaryAttributes.Intelligence;
            int actualPrimAtribuesStrenght = warrior.BasePrimaryAttributes.Strength;
            int actualPrimAtribuesDexterity = warrior.BasePrimaryAttributes.Dexterity;

            int actualSecondaryAttributesHealth = warrior.SecondaryAttribute.Health;
            int actualSecondaryAttributesArmorRating = warrior.SecondaryAttribute.ArmorRating;
            int actualSecondaryAttributesElementResistance = warrior.SecondaryAttribute.ElementalResistance;


            //Assert
            Assert.Equal(expectedLevel, actualLevel);

            Assert.Equal(expectedPrimAttributes.Vitality, actualPrimAtribuesVitality);
            Assert.Equal(expectedPrimAttributes.Intelligence, actualPrimAtribuesIntelligence);
            Assert.Equal(expectedPrimAttributes.Strength, actualPrimAtribuesStrenght);
            Assert.Equal(expectedPrimAttributes.Dexterity, actualPrimAtribuesDexterity);

            Assert.Equal(expectedSecondaryAttributes.Health, actualSecondaryAttributesHealth);
            Assert.Equal(expectedSecondaryAttributes.ArmorRating, actualSecondaryAttributesArmorRating);
            Assert.Equal(expectedSecondaryAttributes.ElementalResistance, actualSecondaryAttributesElementResistance);

        }
        #endregion

        #region Increase character attributes when they level up
        [Fact]
        public void WarriorLevelUP_LevelUp_ReturnIncreasedPrimaryAttributes()
        {
            //Arrange
            int lhs = 1;

            warrior.BasePrimaryAttributes.Vitality += warrior.LevelUp.Vitality;
            warrior.BasePrimaryAttributes.Intelligence += warrior.LevelUp.Intelligence;
            warrior.BasePrimaryAttributes.Strength += warrior.LevelUp.Strength;
            warrior.BasePrimaryAttributes.Dexterity += warrior.LevelUp.Dexterity;

  
            //Act
            warrior.LevelUP(lhs);
            int actualPrimAtribuesVitality = warrior.TotalPrimaryAttributes.Vitality;
            int actualPrimAtribuesIntelligence = warrior.TotalPrimaryAttributes.Intelligence;
            int actualPrimAtribuesStrenght = warrior.TotalPrimaryAttributes.Strength;
            int actualPrimAtribuesDexterity = warrior.TotalPrimaryAttributes.Dexterity;


            //Assert
            Assert.Equal(warrior.BasePrimaryAttributes.Vitality, actualPrimAtribuesVitality);
            Assert.Equal(warrior.BasePrimaryAttributes.Intelligence, actualPrimAtribuesIntelligence);
            Assert.Equal(warrior.BasePrimaryAttributes.Strength, actualPrimAtribuesStrenght);
            Assert.Equal(warrior.BasePrimaryAttributes.Dexterity, actualPrimAtribuesDexterity);
        }

        [Fact]
        public void RoguerLevelUP_LevelUp_ReturnIncreasedPrimaryAttributes()
        {
            //Arrange
            int lhs = 1;

            rogue.BasePrimaryAttributes.Vitality += rogue.LevelUp.Vitality;
            rogue.BasePrimaryAttributes.Intelligence += rogue.LevelUp.Intelligence;
            rogue.BasePrimaryAttributes.Strength += rogue.LevelUp.Strength;
            rogue.BasePrimaryAttributes.Dexterity += rogue.LevelUp.Dexterity;


            //Act
            rogue.LevelUP(lhs);
            int actualPrimAtribuesVitality = rogue.TotalPrimaryAttributes.Vitality;
            int actualPrimAtribuesIntelligence = rogue.TotalPrimaryAttributes.Intelligence;
            int actualPrimAtribuesStrenght = rogue.TotalPrimaryAttributes.Strength;
            int actualPrimAtribuesDexterity = rogue.TotalPrimaryAttributes.Dexterity;


            //Assert
            Assert.Equal(rogue.BasePrimaryAttributes.Vitality, actualPrimAtribuesVitality);
            Assert.Equal(rogue.BasePrimaryAttributes.Intelligence, actualPrimAtribuesIntelligence);
            Assert.Equal(rogue.BasePrimaryAttributes.Strength, actualPrimAtribuesStrenght);
            Assert.Equal(rogue.BasePrimaryAttributes.Dexterity, actualPrimAtribuesDexterity);
        }

        [Fact]
        public void RangerLevelUP_LevelUp_ReturnIncreasedPrimaryAttributes()
        {
            //Arrange
            int lhs = 1;

            ranger.BasePrimaryAttributes.Vitality += ranger.LevelUp.Vitality;
            ranger.BasePrimaryAttributes.Intelligence += ranger.LevelUp.Intelligence;
            ranger.BasePrimaryAttributes.Strength += ranger.LevelUp.Strength;
            ranger.BasePrimaryAttributes.Dexterity += ranger.LevelUp.Dexterity;


            //Act
            ranger.LevelUP(lhs);
            int actualPrimAtribuesVitality = ranger.TotalPrimaryAttributes.Vitality;
            int actualPrimAtribuesIntelligence = ranger.TotalPrimaryAttributes.Intelligence;
            int actualPrimAtribuesStrenght = ranger.TotalPrimaryAttributes.Strength;
            int actualPrimAtribuesDexterity = ranger.TotalPrimaryAttributes.Dexterity;


            //Assert
            Assert.Equal(ranger.BasePrimaryAttributes.Vitality, actualPrimAtribuesVitality);
            Assert.Equal(ranger.BasePrimaryAttributes.Intelligence, actualPrimAtribuesIntelligence);
            Assert.Equal(ranger.BasePrimaryAttributes.Strength, actualPrimAtribuesStrenght);
            Assert.Equal(ranger.BasePrimaryAttributes.Dexterity, actualPrimAtribuesDexterity);
        }

        [Fact]
        public void MageLevelUP_LevelUp_ReturnIncreasedPrimaryAttributes()
        {
            //Arrange
            int lhs = 1;

            mage.BasePrimaryAttributes.Vitality += mage.LevelUp.Vitality;
            mage.BasePrimaryAttributes.Intelligence += mage.LevelUp.Intelligence;
            mage.BasePrimaryAttributes.Strength += mage.LevelUp.Strength;
            mage.BasePrimaryAttributes.Dexterity += mage.LevelUp.Dexterity;


            //Act
            mage.LevelUP(lhs);
            int actualPrimAtribuesVitality = mage.TotalPrimaryAttributes.Vitality;
            int actualPrimAtribuesIntelligence = mage.TotalPrimaryAttributes.Intelligence;
            int actualPrimAtribuesStrenght = mage.TotalPrimaryAttributes.Strength;
            int actualPrimAtribuesDexterity = mage.TotalPrimaryAttributes.Dexterity;


            //Assert
            Assert.Equal(mage.BasePrimaryAttributes.Vitality, actualPrimAtribuesVitality);
            Assert.Equal(mage.BasePrimaryAttributes.Intelligence, actualPrimAtribuesIntelligence);
            Assert.Equal(mage.BasePrimaryAttributes.Strength, actualPrimAtribuesStrenght);
            Assert.Equal(mage.BasePrimaryAttributes.Dexterity, actualPrimAtribuesDexterity);
        }
        #endregion

        #region Worrior secondary attributes when they level upp
        [Fact]
        public void WarriorAllSecondaryAttributes_LevelUp_ReturnIncreasedSecondaryAttributes()
        {
            //Arrange
            int lhs = 1;
            int ExpectedSecAttributHealth = 150;
            int ExpectedSecAttributArmorRating = 12;
            int ExpectedSecAttributElementResistance = 2;

            //Act
            warrior.LevelUP(lhs);
            int actualSecAttribueHealth = warrior.SecondaryAttribute.Health;
            int actualSecAttribueArmorRating = warrior.SecondaryAttribute.ArmorRating;
            int actualSecAttribueElementResistance = warrior.SecondaryAttribute.ElementalResistance;


            //Assert
            Assert.Equal(ExpectedSecAttributHealth, actualSecAttribueHealth);
            Assert.Equal(ExpectedSecAttributArmorRating, actualSecAttribueArmorRating);
            Assert.Equal(ExpectedSecAttributElementResistance, actualSecAttribueElementResistance);
        }
        #endregion

    }
}
