﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using FirstAssignment;
using FirstAssignment.Characters;
using static FirstAssignment.Weapon;
using static FirstAssignment.Armor;

namespace FirstAssignmentTest
{
    public class ItemsAndEquipment
    {
        Warrior warrior = new Warrior();
        Ranger ranger = new Ranger();


        #region Equip weapon and armor for higher level
        [Fact]
        public void EquipWeapon_EquipHighLevelWeapon_ReturnInvalidWeaponException()
        {
            //Arrange
            Weapon testAxe = new Weapon()
            {
                ItemName = "Common axe",
                ItemLevel = 2,
                ItemSlot = Slot.Weapon,
                WeaponType = WeaponTypes.Axes,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, Attackspeed = 1.1 }
            };

            //Act & Assert
            Assert.Throws<InvalidWeaponType>(() => warrior.EquipWeapon(testAxe));
        }

        [Fact]
        public void EquipArmor_EquipHighLevelArmor_ReturnInvalidWeaponException()
        {
            //Arrange
            Armor PlateBody = new Armor()
            {
                ItemName = "Common plate body armor",
                ItemLevel = 2,
                ItemSlot = Slot.Body,
                armorType = ArmorTypes.Plate,
                attributes = new PrimaryAttribute() { Vitality = 2, Strength = 1 }
            };

            //Act & Assert
            Assert.Throws<InvalidArmorType>(() => warrior.EquipArmor(PlateBody));
        }
        #endregion

        #region Equip wrong type of armor and weapon 
        [Fact]
        public void EquipArmor_EquipWrongTypOfArmor_ReturnInvalidWeaponException()
        {
            //Arrange
            Armor ClothHead = new Armor()
            {
                ItemName = "Common cloth head armor",
                ItemLevel = 1,
                ItemSlot = Slot.Head,
                armorType = ArmorTypes.Cloth,
                attributes = new PrimaryAttribute() { Vitality = 1, Strength = 5 }
            };

            //Act & Assert
            Assert.Throws<InvalidArmorType>(() => warrior.EquipArmor(ClothHead));
        }

        [Fact]
        public void EquipWepon_EquipWrongTypOfWeapon_ReturnInvalidWeaponException()
        {
            //Arrange
            Weapon testBow = new Weapon()
            {
                ItemName = "Common bow",
                ItemLevel = 1,
                ItemSlot = Slot.Weapon,
                WeaponType = WeaponTypes.Bows,
                WeaponAttributes = new WeaponAttributes() { Damage= 12, Attackspeed=0.8 }
            };

            //Act & Assert
            Assert.Throws<InvalidWeaponType>(() => warrior.EquipWeapon(testBow));
        }

        #endregion

        #region Equip valid armor and return string uppdate 
        [Fact]
        public void EquipArmor_ValidArmor_returnMessageNewArmorIsEquipped()
        {
            //arrange
            Armor MailBody = new Armor()
            {
                ItemName = "common mail body armor",
                ItemLevel = 1,
                ItemSlot = Slot.Body,
                armorType = ArmorTypes.Mail,
                attributes = new PrimaryAttribute() { Vitality=2, Strength=1}
            };

            string ExpectedMessage = "New armor equiped";
            string actual;

            //Act
            actual = ranger.EquipArmor(MailBody);

            //Assert
            Assert.Equal(ExpectedMessage, actual);

        }

        [Fact]
        public void EquipWeapon_ValidArmor_returnMessageNewArmorIsEquipped()
        {
            //arrange
            Weapon TestBow = new Weapon()
            {
                ItemName = "common bow",
                ItemLevel = 1,
                ItemSlot = Slot.Weapon,
                WeaponType = WeaponTypes.Bows,
                WeaponAttributes = new WeaponAttributes() { Damage = 12, Attackspeed=0.8 }
            };

            string ExpectedMessage = "New weapon equiped";
            string actual;

            //Act
            actual = ranger.EquipWeapon(TestBow);

            //Assert
            Assert.Equal(ExpectedMessage, actual);

        }
        #endregion

        #region Calculate characterDPS, with no weapon, valid weapon and valid weapon and armor
        [Fact]
        public void CharacterDPS_NoWeapon_returnBaseDPS()
        {
            //arrange
            
            warrior.Level = 1;
            double ExpectedDPS = 1 * (1 + (5 / 100));

            //Act
            double actual = warrior.CharacterDPS();


            //Assert
            Assert.Equal(ExpectedDPS, actual);

        }

        [Fact]
        public void CharacterDPS_ValidWeapon_returnCharacterDPS()
        {
            //arrange
            Weapon Axe = new Weapon()
            {
                ItemName = "Common Axe",
                ItemLevel = 1,
                ItemSlot = Slot.Weapon,
                WeaponType = WeaponTypes.Axes,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, Attackspeed = 1.1 }
            };
            warrior.Level = 1;
            double ExpectedDPS = (7.0 * 1.1) * (1.0 + (5.0 / 100.0));

            //Act
            warrior.EquipWeapon(Axe);
            double actual = warrior.CharacterDPS();


            //Assert
            Assert.Equal(ExpectedDPS, actual);

        }

        [Fact]
        public void CharacterDPS_ValidWeaponAndArmor_returnCharacterDPS()
        {
            //arrange
            Weapon Axe = new Weapon()
            {
                ItemName = "Common Axe",
                ItemLevel = 1,
                ItemSlot = Slot.Weapon,
                WeaponType = WeaponTypes.Axes,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, Attackspeed = 1.1 }
            };

            Armor PlateBody = new Armor()
            {
                ItemName = "Common plate body armor",
                ItemLevel = 1,
                ItemSlot = Slot.Body,
                armorType = ArmorTypes.Plate,
                attributes = new PrimaryAttribute() { Vitality = 2, Strength = 1 }
            };

            warrior.Level = 1;
            double ExpectedDPS = (7.0 * 1.10) * (1.0 + ((5.0 + 1.0) / 100.0));
            
            //Act
            warrior.EquipWeapon(Axe);
            warrior.EquipArmor(PlateBody);
            double actual = warrior.CharacterDPS();


            //Assert
            Assert.Equal(ExpectedDPS, actual);

        }
        #endregion
    }
}
